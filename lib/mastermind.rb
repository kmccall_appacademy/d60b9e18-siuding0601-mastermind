require 'byebug'
class Code
  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  PEGS = {"R" => "Red", "G" => "Green", "B" => "Blue", "Y" => "Yellow", "O" => "Orange", "P" => "Purple"}

  def self.random
    Code.new(PEGS.keys.to_a.sample(4))
  end

  def self.parse(input)
    code = input.upcase.split("")
    code.each do |al|
      unless PEGS.keys.include?(al)
        raise "No such colors!"
      end
    end
    Code.new(code)
  end

  def [](input)
    @pegs[input]
  end

  def exact_matches(other_code)
    count = 0
    self.pegs.each_with_index do |al, idx|
      if other_code.pegs[idx] == al
        count += 1
      end
    end
    count
  end

  def near_matches(other_code)
    overlap = self.pegs & other_code.pegs
    overlap.count - exact_matches(other_code)
  end

  def ==(code)
    if code.class != Code
      return false
    elsif self.pegs == code.pegs
      return true
    end
  end

end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
  end

  def get_guess
    puts "What is your guess?"
    begin
      Code.parse(gets.chomp)
    rescue
      puts "Invalid guess, guess again!"
      retry
    end
  end

  def display_matches(code)
    puts "You have #{secret_code.exact_matches(code)} exact match(es)"
    puts "You have #{secret_code.near_matches(code)} near match(es)"
  end

  def play
    10.times.each do |i|
      guess = self.get_guess
      p "You have #{9-i} guesses left"
      if secret_code.==(guess)
         return p "You got it"
      else
        self.display_matches(guess)
      end
    end
    p "You ran out of guesses!"
  end

end

game = Game.new
game.play
